﻿#include <iostream>

class Animal {
public:
    virtual void voice() {
        std::cout << "Voice." << std::endl;
    }
};

class Cat : public Animal {
public:
    void voice() override {
        std::cout << "Meow!" << std::endl;
    }
};
class Dog : public Animal {
public:
    void voice() override {
        std::cout << "Woof!" << std::endl;
    }
};
class Cow : public Animal {
public:
    void voice() override {
        std::cout << "Moo!" << std::endl;
    }
};

int main() {

    Animal* animals[5];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Cow();
    animals[3] = new Cat();
    animals[4] = new Dog();

    for (const auto& animal : animals) {
        animal->voice();
    }

    for (const auto& animal : animals) {
        delete animal;
    }

    return 0;
}